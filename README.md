# Project Mattack

An AI that knows how to ebay good

MVP: A autonomous bot that can buy and resell games for a profit (with minimal risk)

High level of abstraction: a system of specialized neural nets that feed into a parent neural net that determines a viability score based on a set minimal profit margin allowed

Neural Nets: \
**Mattack**: takes in all variable data collected from the other neural nets about the possible purchase in question then decides viability of the purchase

**QoP**(quality of product): neural net trained to distinguish between good and bad quality on a given poster submission 

**Every Seller’s Reliability Bot (ESRB)**: neural net that looks at seller ratings and types of items sold to decide if the seller is worth interacting with  (extension saves sellers with high reliability score)

**Item Priority Bot(IPB)**: Quality of product but based on general “trading trends” rather than specific seller (roughly sales analytics ) 

**Media analytics**: follows media trends(trends being game rating, google searches, social media hits, game release date, youtube analytics ) to follow search traffic for a marked game to be purchased 

**Offer Maker**: decides what offer to make and how to move forward with counter offers.

**Maximum Sale Rating for Profit (MSRP): **Decides best list price for purchased game based on IPB (includes list adjustment) 

**Best Offer and Counter Offer bot(BOCO)**: decides if to accept best offer, give counter offer, or ignore.

Problems to solve: alot

-Variable cost of games on the market

-Variable quality of games on the market

-Amount of time expected before an item sells

-Amount of games at x quality is on the market
