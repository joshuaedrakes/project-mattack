import csv, random
from item import Item
from ebaysdk.finding import Connection as finding
from bs4 import BeautifulSoup
from ebaysdk.merchandising import Connection
from EbaySpider import getImages, getAvgSellPrice 
from image_download import download_images
import re

ID_APP = 'KevinGro-MattackT-PRD-6419bd1f9-42087762'
games = []

with open('search_list_3ds.csv') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    line_count = 0
    for row in csv_reader:
        games.append(row[0])

Keywords = random.choice(games) + " 3ds"
print(f"Randomly chose {Keywords}")
avgPrice = getAvgSellPrice(Keywords)
api = finding(appid=ID_APP, config_file=None)
api_request = { 'keywords': Keywords}
response = api.execute('findItemsByKeywords', api_request)
soup = BeautifulSoup(response.content,'lxml')
totalentries = int(soup.find('totalentries').text)
items = soup.find_all('item')

print("Itemcount", len(items))
scraped = {}

for itempage in items:
    scraped[itempage.itemid.string] = Item(
        itemId=itempage.itemid.string, 
        title=itempage.title.string, 
        globalId=itempage.globalid.string, 
        autopay=itempage.autopay.string, 
        country=itempage.country.string, 
        shippingCost= itempage.shippinginfo.shippingservicecost.string if itempage.shippinginfo.shippingservicecost != None else None,
        price=itempage.currentprice.string, 
        sellingState=itempage.sellingstatus.sellingstate.string, 
        bestOfferEnabled=itempage.listinginfo.bestofferenabled.string, 
        buyItNow=itempage.listinginfo.buyitnowavailable.string, 
        listType=itempage.listinginfo.listingtype.string, 
        condition=itempage.condition.string if itempage.condition != None else None, 
        url=itempage.viewitemurl.string
        )

for items in scraped:
    ebayItem = scraped[items]
    print("Item id: ",ebayItem.itemId)
    print("Autopay enabled: ",ebayItem.autopay)
    print("Country: ",ebayItem.country)
    print("Shipping cost: ",ebayItem.shippingCost)
    print("Price: ",ebayItem.price)
    print("Selling state: " ,ebayItem.sellingState)
    print("Best offer enabled: ",ebayItem.bestOfferEnabled)
    print("Buy it now enabled: ",ebayItem.buyItNow)
    print("List Type: ",ebayItem.listType)
    print("Condition: ",ebayItem.condition)
    print(ebayItem.url)
    print("")

selectedVar = input("Enter the Item id of the Item you want to inspect: ")
getImages(scraped[selectedVar].url,scraped[selectedVar])

urls_to_scrape = []
for value in [ii for ii in scraped[selectedVar].imgUrls if "l64" in ii]:
    urls_to_scrape.append(re.sub(r'l64.jpg', r'l500.jpg', value))

urls_to_scrape = list(set(urls_to_scrape))

download_images(urls_to_scrape)
print("The average price is: " + avgPrice)
