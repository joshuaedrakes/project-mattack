from urllib.request import Request, urlopen
from bs4 import BeautifulSoup
from item import Item
import requests

def getImages(link,item):
    req = Request(link, headers={'User-Agent': 'Mozilla/5.0'})
    webpage = urlopen(req).read()
    with requests.Session() as c:
        soup = BeautifulSoup(webpage,'html5lib')
        imageTags = soup.findAll('img')
        imageUrls = []
        for image in range(0, len(imageTags)):
            url = imageTags[image]['src']
            if((("l300" in url) or ("l64" in url) or ("l500" in url)) and ("roversync" not in url)):
                imageUrls.append(url)
        item.imgUrls = imageUrls
        
        #TODO return list of array 

def getAvgSellPrice(gameName):
    nameKeywords = gameName.split()
    urlPt1 = "https://www.ebay.com/sch/i.html?_from=R40&_nkw="
    urlPt2 =""
    urlPt3 ="&_sacat=0&rt=nc&LH_Complete=1"
    for i in range(0,len(nameKeywords)):
        if (i < len(nameKeywords) - 1):
            urlPt2 += (nameKeywords[i] +'+')
        else:
             urlPt2 += (nameKeywords[i])
    link = urlPt1 + urlPt2 +urlPt3
    # soldItemTitles = []
    soltItemPrices = []
    req = Request(link, headers={'User-Agent': 'Mozilla/5.0'})
    webpage = urlopen(req).read()
    with requests.Session() as c:
        soup = BeautifulSoup(webpage,'html5lib')
        # for el in soup.findAll('h3', attrs={'class':'s-item__title s-item__title--has-tags'}):
        #     soldItemTitles.append(el.get_text())
        for el in soup.findAll('span', attrs={'class':'s-item__price'}):
            if("to" not in el.get_text().replace('$','')):
                soltItemPrices.append(float(el.get_text().replace('$','')))
    return "{:.2f}".format(sum(soltItemPrices) / len(soltItemPrices))


