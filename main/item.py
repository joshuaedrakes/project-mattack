class Item:
    def __init__(self, itemId=None, title=None, globalId=None, autopay=None, country=None, shippingCost=None, price=None, sellingState=None, bestOfferEnabled=None, buyItNow=None, listType=None, condition=None, url=None, imgUrls=None):
        self.itemId = itemId
        self.title = title
        self.globalId = globalId
        self.autopay = autopay
        self.country = country
        self.shippingCost = shippingCost
        self.price = price
        self.sellingState = sellingState
        self.bestOfferEnabled = bestOfferEnabled
        self.buyItNow = buyItNow
        self.listType = listType
        self.condition = condition
        self.url = url
        self.imgUrls = imgUrls

    @property
    def itemId(self):
        return self._itemId
    @itemId.setter
    def itemId(self, itemId):
        self._itemId = itemId

    @property
    def title(self):
        return self._title
    @title.setter
    def title(self, title):
        self._title = title

    @property
    def globalId(self):
        return self._globalId
    @globalId.setter
    def globalId(self, globalId):
        self._globalId = globalId

    @property
    def autopay(self):
        return self._autopay
    @autopay.setter
    def autopay(self, autopay):
        self._autopay = autopay

    @property
    def country(self):
        return self._country
    @country.setter
    def country(self, country):
        self._country = country

    @property
    def shippingCost(self):
        return self._shippingCost
    @shippingCost.setter
    def shippingCost(self, shippingCost):
        self._shippingCost = shippingCost

    @property
    def price(self):
        return self._price
    @price.setter
    def price(self, price):
        self._price = price

    @property
    def sellingState(self):
        return self._sellingState
    @sellingState.setter
    def sellingState(self, sellingState):
        self._sellingState = sellingState

    @property
    def buyItNow(self):
        return self._buyItNow
    @buyItNow.setter
    def buyItNow(self, buyItNow):
        self._buyItNow = buyItNow

    @property
    def listType(self):
        return self._listType
    @listType.setter
    def listType(self, listType):
        self._listType = listType

    @property
    def condition(self):
        return self._condition
    @condition.setter
    def condition(self, condition):
        self._condition = condition

    @property
    def url(self):
        return self._url
    @url.setter
    def url(self, url):
        self._url = url
    
    @property
    def imgUrls(self):
        return self._imgUrls
    @imgUrls.setter
    def imgUrls(self, imgUrls):
        self._imgUrls = imgUrls
