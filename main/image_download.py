import requests # to get image from the web
import shutil # to save it locally
import pathlib
basepath = pathlib.Path(__file__).parent.absolute()

def download_images(image_urls):
    counter = 0
    filenames = []

    for url in image_urls:

        # Open the url image, set stream to True, this will return the stream content.
        r = requests.get(url, stream = True)

        # Check if the image was retrieved successfully
        if r.status_code == 200:
            # Set decode_content value to True, otherwise the downloaded image file's size will be zero.
            r.raw.decode_content = True
    
            # Open a local file with wb ( write binary ) permission.

            with open(f"{basepath}/temp/{counter}.png",'wb') as f:
                shutil.copyfileobj(r.raw, f)
        
            print('Image sucessfully Downloaded: ', f"{basepath}/temp/{counter}.png")
            filenames.append(f"{basepath}/temp/{counter}.png")
            counter += 1
        else:
            print(f'Failed to download {url}')

    return filenames
